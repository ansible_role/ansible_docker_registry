## Ansible Docker Registry

Role provides installation of 
[private docker registry](https://docs.docker.com/registry). Registry 
container will run under unprivileged user, and will be proxied by nginx.
Nginx configuration include ssl section, so certificate and private key can
 be uploaded by this role, or just configured as a filepaths (when they 
 already exists on the target host).
 
As we know, docker registry can not work without docker engine. Installation 
of docker engine on target host is provided by one of dependency role of current
 role.
 
On one target host multiple registries able to be located, because each 
registry can be runned under it`s user.
 

### Requirements
```bash
cd /path/to/this/role
ansible-galaxy install -r requirements.yml
```


### Usage
```bash
cd /path/to/this/role
ansible-playbook -i inventory.yml playbook.yml -vv
```
*You have to prepare `inventory.yml` before command running.*


### Inventory example
```yaml
docker-registry:
  hosts:
    
    # ip address of target host    
    1.2.3.4:
      ansible_user: root
      ansible_ssh_private_key_file: /path/to/private/ssh/folder/id_rsa

  vars:
    # we want to install nginx during role evaluation
    docker_registry_nginx_install: yes  
    
    # domain of our private registry
    docker_registry_nginx_domain: docker.example.com 
    
    # we want to use ssl certificate and private key
    docker_registry_nginx_ssl: yes
    
    # we want to upload ssl certificate and private key
    docker_registry_nginx_ssl_upload: yes
    
    # we have ssl certificate and private key files and we copy them to "files"
    # folder of this role
    docker_registry_nginx_ssl_certificate: "{{ inventory_dir }}/files/fullchain.pem"
    docker_registry_nginx_ssl_private_key: "{{ inventory_dir }}/files/privatekey.pem"
```